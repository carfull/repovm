package com.citi.spring.demo3.service;

import java.util.Collection;

import com.citi.spring.demo3.entities.Car;

public interface CarService {

    void addCar(Car c);
    Collection<Car> getCars();
    
}