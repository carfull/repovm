package com.citi.spring.demo3.service;

import java.util.Collection;

import com.citi.spring.demo3.entities.Car;
import com.citi.spring.demo3.repo.CarRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class CarServiceImple implements CarService {


    //inject the mogo repo!

    @Autowired
    private CarRepository repo;




    @Override
    public void addCar(Car c) {
        repo.insert(c);

    }

    @Override
    public Collection<Car> getCars() {
       
        return repo.findAll();
    }


    
}