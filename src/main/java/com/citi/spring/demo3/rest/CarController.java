package com.citi.spring.demo3.rest;

import java.util.Collection;



import com.citi.spring.demo3.entities.Car;
import com.citi.spring.demo3.service.CarService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cars") //because we want a URL
public class CarController {
    
    //inject the service because its layered
    @Autowired
    private CarService carService;

    @RequestMapping(method = RequestMethod.GET)
    public Collection<Car> getCars() {
        return carService.getCars();
    }


    @RequestMapping(method=RequestMethod.POST)
    public void addCar(@RequestBody Car c){
            carService.addCar(c);
    }
}