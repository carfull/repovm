package com.citi.spring.demo3.repo;

import com.citi.spring.demo3.entities.Car;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CarRepository extends MongoRepository<Car, ObjectId> {

    
}